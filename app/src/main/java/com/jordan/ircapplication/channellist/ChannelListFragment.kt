package com.jordan.ircapplication.channellist

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.jordan.ircapplication.IrcApplication
import com.jordan.ircapplication.R
import com.jordan.ircapplication.model.Channel
import com.jordan.ircapplication.mvp.BaseFragment
import com.jordan.ircapplication.mvp.BasePresenter
import com.jordan.ircapplication.mvp.BaseView
import org.jetbrains.anko.find
import org.jetbrains.anko.support.v4.find
import javax.inject.Inject

class ChannelListFragment : BaseFragment<ChannelListView, ChannelListPresenter>(), ChannelListView {

    lateinit var recyclerview: RecyclerView
    lateinit var fab: FloatingActionButton

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater!!.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerview = find<RecyclerView>(R.id.recyclerview)
        fab = find<FloatingActionButton>(R.id.fab)

        fab.setOnClickListener { Toast.makeText(context, "FAB Clicked", Toast.LENGTH_SHORT) }
        recyclerview.layoutManager = LinearLayoutManager(context)

        presenter.load()
    }

    override fun setChannels(channels: List<Channel>) {
        recyclerview.adapter = ChannelListViewAdapter(channels)
    }

    override fun createPresenter(): ChannelListPresenter {
        return DaggerChannelListComponent.builder()
                .applicationComponent((activity.application as IrcApplication).applicationComponent)
                .channelListModule(TestChannelListModule())
                .build()
                .createPresenter()
    }
}

class ChannelListViewAdapter(val channels: List<Channel>) : RecyclerView.Adapter<ChannelListViewHolder>() {
    override fun getItemCount(): Int {
        return channels.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ChannelListViewHolder? {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.channel_row, parent, false)
        return ChannelListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChannelListViewHolder?, position: Int) {
        val chan = channels[position]
        holder?.topic?.text = chan.topic
        holder?.channel?.text = chan.name
    }
}

class ChannelListViewHolder(v: View) : RecyclerView.ViewHolder(v) {
    val topic = v.find<TextView>(R.id.topic)
    val channel = v.find<TextView>(R.id.channel)
}

interface ChannelListView : BaseView {
    fun setChannels(channels: List<Channel>)
}

class ChannelListPresenter @Inject constructor(val channels: List<Channel>) : BasePresenter<ChannelListView>() {

    fun load() {
        view.get().setChannels(channels)
    }

}