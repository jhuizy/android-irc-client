package com.jordan.ircapplication.channellist

import com.jordan.ircapplication.dagger.ApplicationComponent
import com.jordan.ircapplication.model.Channel
import dagger.Component
import dagger.Module
import dagger.Provides

@Module
open class ChannelListModule(val channels: List<Channel>) {

    @Provides
    fun provideChannels() = channels;

}

class TestChannelListModule : ChannelListModule(
        arrayListOf(
                Channel("#Chan1", "This is a random topic I made"),
                Channel("#LongChanName", "dafljds l;dsjfl dsjfldsjfdlsf"),
                Channel("#ChanName3", "Hello welcome to this channel it is nice to meet you finally")
        )
)

@Component(modules = arrayOf(ChannelListModule::class), dependencies = arrayOf(ApplicationComponent::class))
interface ChannelListComponent {
    fun createPresenter(): ChannelListPresenter
}

