package com.jordan.ircapplication.model

data class Channel(val name: String, val topic: String)