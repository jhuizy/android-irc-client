package com.jordan.ircapplication.mvp

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.lang.ref.WeakReference

interface BaseView {
    // default view stuff
}

abstract class BaseFragment<V : BaseView, P : BasePresenter<V>> : Fragment(), BaseView {

    val presenter by lazy { createPresenter() }

    abstract fun createPresenter(): P

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        presenter.view = WeakReference(this as V)
        return super.onCreateView(inflater, container, savedInstanceState)
    }
}

open class BasePresenter<V : BaseView> {

    lateinit var view: WeakReference<V>

}