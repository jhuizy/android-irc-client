package com.jordan.ircapplication.dagger

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.jordan.ircapplication.IrcApplication
import dagger.Component
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(var application: IrcApplication) {

    @Provides
    fun provideApplication() = application

    @Provides
    fun provideApplicationContext() = application.applicationContext

    @Provides
    fun provideSharedPrefs(context: Context) = PreferenceManager.getDefaultSharedPreferences(context)

}

@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun getApplicationContext(): Context
    fun getSharedPreferences(): SharedPreferences
    fun getApplication(): IrcApplication

}