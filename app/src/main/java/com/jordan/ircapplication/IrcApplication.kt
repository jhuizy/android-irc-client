package com.jordan.ircapplication

import android.app.Application
import com.jordan.ircapplication.dagger.ApplicationModule
import com.jordan.ircapplication.dagger.DaggerApplicationComponent

class IrcApplication : Application() {

    val applicationComponent by lazy {
        DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
    }
}

